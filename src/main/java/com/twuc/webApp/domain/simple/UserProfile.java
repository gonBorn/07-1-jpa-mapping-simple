package com.twuc.webApp.domain.simple;

// TODO:
//
// 请创建一个 UserProfile entity。其对应的数据库表的 schema 必须满足如下 SQL 的要求：
//
// table: user_profile
// +────────────────+──────────────+───────────────────────────────+
// | column         | description  | additional                    |
// +────────────────+──────────────+───────────────────────────────+
// | id             | bigint       | auto_increment, primary key   |
// | name           | varchar(64)  | not null                      |
// | year_of_birth  | smallint     | not null                      |
// +────────────────+──────────────+───────────────────────────────+
//
//  请补全如下的代码，你可以新建各种方法、构造器，添加各种 annotation。但是添加的东西越少越好。
//
// <--start-

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;

@Entity
public class UserProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Short yearOfBirth;

    public static UserProfile of(String name, short yearOfBirth) {
        return new UserProfile(name, yearOfBirth);
    }

    public UserProfile() {
    }

    public UserProfile(String name, Short yearOfBirth) {
        // 请实现该构造函数
        this.name = name;
        this.yearOfBirth = yearOfBirth;
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Short getYearOfBirth() {
        return this.yearOfBirth;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setYearOfBirth(Short yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }
}
// --end-->