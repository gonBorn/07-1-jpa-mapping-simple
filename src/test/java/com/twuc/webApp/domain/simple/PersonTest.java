package com.twuc.webApp.domain.simple;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class PersonTest {
    @Autowired
    private PersonRepository repository;

    @Autowired
    private EntityManager entityManager;

    @Test
    void test_1() {
        Person person = new Person("du");
        repository.save(person);
        entityManager.flush();
        entityManager.clear();
        Optional<Person> p = repository.findById(1);
        assertThat(p.isPresent()).isTrue();
        assertThat(p.get().getName()).isEqualTo("du");
    }
}
